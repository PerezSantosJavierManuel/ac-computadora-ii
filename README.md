# COMPUTADORAS ELECTRÓNICAS
``` plantuml
@startmindmap
*[#lightgreen] COMPUTADORAS ELECTRÓNICAS 
 * Son máquinas capaces de efectuar una secuencia de\noperaciones mediante un programa, de manera que\nrealiza un proceso de datos de entrada y obtiene\ndatos de salida.
  *[#Orange] Harvard Mark 1
   * Construida por IBM en 1944 durante la\nsegunda guerra mundial.
   *_ Compuesta por 
    * 765000 componentes.
    * 8 km de cable.
    * 1 eje de 15 metros.
   * Ocupaba un motor de 5 HP.
   *_ USOS
    * Se usaba para correr simulaciones y cálculos\npara el proyecto Manhattan.
   * Su piedra angular era el Relé
    * Es un interruptor mecánico controlado eléctricamente.
    *_ SE COMPONE
     * Bobina
      * Es un alambre de cobre aislado y enrollado.
     * Brazo de hierro
     * 2 Contactos
    *_ FUNCIÓN
     * Cuando una corriente eléctrica atraviesa la bobina se\ngenera un campo magnético que atrae al brazo\nmetálico que está conectado a los contactos, los\nune de manera que cierra el circuito.
     * Es similar a una perilla de agua.
    * El problema del Relé es que el brazo encargado de conectar\nel circuito tiene masa, es decir, que genera inercia, lo que hace\nque demore más en ejecutarse.
    * Mientras más relés, hay más posibilidades de averiarse.
   *_ OPERACIONES
    * 3 Sumas o restas por segundo.
    * Multiplicación y divisiones en 15 segundos.
    * Operaciones complejas en minutos.
  *[#Blue] 1904
   * John Ambrose Fleming
    * Desarrolló un componente nuevo eléctrico llamado\nVálvula Termoiónica.
     * VÁLVULA TERMOIÓNICA
      *_ FORMADA POR
       * Bulbo de cristal
       * Filamento
       * Ánodo
       * Cátodo
      *_ FUNCIÓN
       * Inicia el proceso de emisión termoiónica.
        * El filamento se calienta permitiendo el flujo de eléctrones de\nánodo a cátodo, fluyendo en una sola dirección.
  *[#lightblue] 1906
   * Lee De Forest
    * Inventor americano.
    * Agregó un tercer electrodo llamado\n"Electrodo de control".
     * ELECTRODO DE CONTROL
      * Este está sistuado entre el ánodo y el cátodo,\nbasado en el diseño de Fleming.
      * Si se le aplica una carga positiva o negativa, permite el\nflujo, en caso contrario detiene la corriente.
      * Cumple con las mismas funciones de los relés,\nsin dañarse con tanta frecuencia.
      *_ CAPACIDAD
       * Podían cambiar de estado miles de veces\npor segundo.
      * Fueron la base para la radio, teléfonos de larga distancia\ny otros dispositivos por 50 años.
      * Eran muy frágiles.
    * Se soluciona el problema del interruptor eléctrico.
    * Los tubos de vacío eran costosos, y las computadoras\nrequerían cientos de miles.
    * En 1940 se volvieron más factibles los tubos de vacío.
  *[#Green] Colossus Mark 1
   * Es la primera computadora electrónica programable.
   * Tuvieron los primeros usos a gran escala de\nlos tubos de vacío en las computadoras.
   * Diseñada por Tommy Flowers.
   * Finalizada en Diciembre de 1943.
   *_ DATO
    * Dos años antes, Alan Turing creó un dispositivo\nelectromecánico llamado "The Bombe".
     * Diseñado para decifrar el código enigma nazi.
     * Técnicamente no era computadora.
   * Situada en Bletchley, Reino Unido.
   *_ USOS
    * Decodificar las comunicaciones nazis.
   *_ CONTENÍA
    * 1600 tubos de vacío.
   * Se construyeron 10 Colossus más, para ayudar en\nlas codificaciones de aquella época.
   *_ CONFIGURACIÓN
    * Mediante una conexión de cientos de cables en un tablero.
    * Su configuración era muy similar a los tableros de\nconmutación que se ocupaban en los teléfonos.
  *[#HotPink] ENIAC
   * Construida en 1946, Universidad de Pensilvania.
   * Por sus siglas
    * Calculadora Integradora Numérica Electrónica (ENIAC).
   * Diseñada por
    * John Mauchly &\nJ. Presper Eckert.
   * Primera computadora programable de propósito general.
   *_ FUNCIONES
    * Podía realizar 5000 sumas y restas de 10 dígitos por segundo.
    * Se calcula que hizo más operaciones aritméticas que\ntoda la humanidad hasta ese momento.
    * Funcionaba la mitad del día.
  *[#AAAAAA] 1947
   * Bell Laboratories
    * John Bardeen, Walter Brattain y William Shockley.
     * Crearon el transistor.
     * Se marca una nueva era para las computadoras.
      * El transistor cumple con las mismas funciones\nque el relé o el tubo de vacío.
      * Están hechas de silicio, lo que no permite la conductividad.
       * DOPING
        * Se agregan elementos al silicio permitiendo la conductividad\nde las corrientes eléctricas.
      * Construido por tres capas de silicio.
       * Colector
       * Emisor
       * Base
       * Cuando hay flujo entre la base y el emisor, también hay un flujo\nentre el emisor el colector, funcionando como un\ninterruptor eléctrico.
      * Eran sólidos, muy resistibles.
  *[#7FFFD4] IBM-608
   * Creada en 1957
   * Primera computadora disponible comercialmente, basado en transistores.
   * Realizaba 4500 sumas, 80 divisiones o multiplicaciones\npor segundo.
   * Empezó a usar transistores en todos sus productos.
   * Empezaron a distribuirse y venderse en oficinas\ny en los hogares.
   * Actualmente, los transistores tienen un\ntamaño menor a los 50 nn.
    * Son diminutos y muy rápidos.
    * Pueden cambiar de estado millones de\nveces por segundo.
    * Incluso funcionan por décadas.
@endmindmap
```
# ARQUITECTURA VON-NEUMANN Y ARQUITECTURA HARVARD
``` plantuml
@startmindmap
*[#lightblue] ARQUITECTURA VON-NEUMANN Y HARVARD
 *[#lightgreen] LEY DE MOORE
  * Establece que la velocidad del procesador o el poder del procesamiento\ntotal de las computadoras se duplica cada año.
   * El transistor de manera interna está\ncompuesta de transistores.
  *_ ELECTRÓNICA
   * Números de transistores por chip llega\na multiplicarse cada año.
   * El costo de los chips no cambia.
   * Cada año y medio se duplica la potencia del\ncálculo sin llegar a modificar el costo.
  *_ PERFORMANCE
   * Incrementa velocidad del procesador.
   * Incrementa capacidad de memoria.
   * La velocidad de memoria corre siempre\ndetrás de la velocidad del procesador.
 *[#AAAAAA] FUNCIONAMIENTO INTERNO DE LAS COMPUTADORAS
  * ANTES
   * Habían sistemas cableados.
    * Se desenchufaban y enchufaban los cables.
    * Así se programaba.
   *_ HABÍA 
    * Entrada de datos.
    * Secuencia de funciones aritméticas/lógicas.
    * Resultados.
   * La programación era en cuanto a hardware.
  * AHORA
   * La programación es mediante software.
   *_ HAY
    * Entrada de datos.
    * Secuencia de funciones lógicas/aritméticas.
    * Tenemos un interprete de funciones.
    * Señales de control.
    * Resultados.
 *[#Red] ARQUITECTURA DE VON-NEUMANN
  * Conocida como arquitectura moderna.
  * Tres partes fundamentales
   * CPU.
   * Memoria.
   * Módulos E/S
  *_ CONECTADAS POR
   * BUSES
    * Buses de control.
    * Buses de dirección.
    * Buses de datos e instrucciones.
  * MODELO
   * Arquitectura de 1945, por el matemático y físico\nJohn Von-Neumann y otros.
   * Los programas y datos se almacenan en la misma memoria\nde lectrua y escritura.
   * Se acceden a los contenidos indicando la posición.
   * Ejecución en secuencia.
   * Representación binaria.
  *_ SURGE
   * Concepto de programa almacenado.
   * Separación de la memoria y la CPU acarreó un problema\nllamado Neumann Bottleneck (Cuello de botella de Neumann).
  * MODELO DE BUS
   * Es un refinamiento del modelo Von-Neumann.
   * Se encarga de reducir las conexiones entre la CPU\ny sus sistemas.
  *_ CONTIENE
   * INSTRUCCIONES
    * Los programas se encuentran localizados en memoria\ny consisten de instrucciones.
    * La CPU se encarga de ejecutar las instrucciones\n(son binarias).
    * Se emplean lenguajes de alto y bajo nivel.
    * Son ejecutadas por la CPU a grandes velocidades.
   * CICLO DE EJECUCIÓN
    * La unidad de control busca la instrucción que sigue\nen la memoria.
    * Se incrementa el registro PC.
    * La instrucción pasa por un proceso de decodificación.
    * Se obtienen los operandos requeridos.
    * La ALU ejecuta y deja los resultados en memoria.
   * CICLO DE INSTRUCCIÓN
    * Busca la dirección de la instrucción.
    * Se decodifica.
    * Se verifica si hay que buscar un dato.
    * Se operan los datos.
    * Se cálcula la dirección del operando.
 *[#Yellow] ARQUITECTURA HARVARD
  * Es también conocida como arquitectura de microcontroladores.
  * Originalmente hacía referencia a las arquitecturas que utilizaban\ndispositivos de almacenamiento físicos separados para instrucciones\ny datos.
  *_ CONTIENE
   * CPU
   * Memoria principal
   * E/S
  * MEMORIAS
   * Cuesta más fabricar una memoria de forma rápida.
   * Es más viable proporcionar una pequeña parte de\nmemoria conocida como CACHÉ.
   * Con el CACHÉ el rendimiento es mayor.
  * SOLUCIÓN HARVARD
   * Las instrucciones y datos se almacenan en CACHÉS separados.
   * Funciona mejor si la frecuencia de lectura de instrucciones\ny datos son aproximadamente la misma.
  *_ SE DIVIDE
   * PROCESADOR
    * Dos unidades principales.
     * Unidad de control.
     * Unidad aritmética y lógica.
   * MEMORIA DE INSTRUCCIONES
    * Aquí se almacenan las instrucciones del programa.
    * El tamaño de palabras se adapta al número de bits\nde instrucciones del microcontrolador.
    * Se implementa utilizando memorias no volátiles.
   * MEMORIA DE DATOS
    * Se almacenan datos utilizados por los programas.
    * Los datos varían continuamente.
    * Es recomendable ocupar la memoria RAM para realizar\noperaciones.
@endmindmap
```
# BASURA ELECTRÓNICA
``` plantuml
@startmindmap
*[#Red] BASURA ELECTRÓNICA
 *[#lightgreen] E-END
  * Cada año son procesadas montañas de basura en este centro de reciclaje.
  * STEVE CHAFITZ
   * Presidente de E-END.
   * Dice que hay oro entre la basura que procesan.
   * Hay protocolos que seguir para reciclar de\nuna forma segura y responsable.
   * Es importante borrar la información antes de\ndeshacerse de los dispositivos.
  * Podemos encontrar metales preciosos como el\noro, cobre, plata, cobalto.
  * Se encargan de destruir los discos duros para\nque no sean utilizados con malas intenciones.
 *[#Yellow] PERÚ GREEN RECYCLING
  * En esta planta se le da un tratamiento a los aparatos\neléctricos y electrónicos.
  * Es necesario colocarse todos los implementos de\nseguridad.
  * Se les da una correcta gestión a los aparatos.
  * PROCESOS
   * Recolección.
   * Se seleccionan desechos según su categoría.
   * Desarmarlos para recuperar los materiales primarios\ncomo metales y plásticos.
   * Creación de nuevos productos.
   * Exportación a otros continentes.
  * Los residuos peligrosos son llevados a un relleno\nde seguridad.
   * Tienen pozas protegidas con membranas especiales\nque evitan que haya derrames.
  * Se da un tratamiento aparte a los resiudos peligrosos.
 *[#lightblue] COSTA RICA
  * SERVICIOS ECOLÓGICOS
   * Esta empresa nació de la idea de un hombre que pensó que\nla basura podía ser una fuente de materia prima.
   * El 80% de residuos que esta empresa recibe, proviene\ndel sector industrial.
   * PROCESO
    * Clasificación.
    * Desmantelamiento.
    * Selección.
    * Preparan los materiales para enviarlos al procesador final.
   * Separan los materiales para esperar a que algún\ncliente lo compre.
 *[#Orange] RECICLAJE EN MÉXICO
  * TECHEMET
   * Reciben el material y lo clasifican.
   * Generan un archivo del cliente y estiman\nun pago.
   * PROCESO
    * Recepción del material eléctrico.
    * Clasificarlo y pesarlo para calcular el pago\npor el producto.
    * Desarmarlo y clasificarlo de acuerdo a sus\ncomponentes.
    * Segregación y limpieza de todas las piezas.
    * Almacenar con su clasificación correspondiente.
    * Exportar para recuperar los metales.
  *[#Lime] REMSA
   * Mediante procesos especificos los empleados se encargan de\ndesarmar cada pieza de los diferentes objetos.
   * Se separan plásticos, cristales, tarjetas, etc.
   * Después se trituran para posteriormente elaborar distintos\nproductos con la materia prima resultante.
   * PRINCIPALES PROCESOS
    * Volver útiles los vidrios de televisores y computadoras,\npues contienen materiales tóxicos.
    * Convertir carcasas de plástico para suelas de zapatos.
   * Lograr recuperar el 90% de materiales.
 *[#Green] RAEE
  * Son los residuos de aparatos eléctricos y electrónicos.
  * Cuando estos llegan a culminar su vida útil se\nconvierten en un RAEE.
  * Ameritan un manejo especial.
  * ¿QUÉ HACEN CUANDO ESTOS APARATOS YA NO SE USAN?
   * Ciertas personas las llegan a regalar.
   * Algunos las coleccionan.
   * Los tiran a la basura.
 *[#HotPink] EXPORTACIONES ILEGALES
  * La basura electrónica es exportada en grandes contenedores\na paises tercer mundistas, donde se aplican métodos peligrosos para\nel medio ambiente.
  * PAISES COMO
   * China.
   * México.
   * India.
   * Donde es desembalada para extraer los metales.
  * Incluso entre estos contenedores hay residuos\nelectrónicos ilegales.
  * ¿POR QUÉ EXISTE EL MERCADO ILEGAL?
   * Porque reciclar de forma segura es muy costoso.
   * Los procesos son muy complicados.
   * Se usan métodos mucho más primitivos y baratos.
  * DATOS CURIOSOS
   * No solo se encuentran metales, también es muy valiosa la\ncantidad de datos que se encuentran en la basura.
   * Los discos duros son separados con fines criminales.
 *[#AAAAAA] DESECHOS ELECTRÓNICOS
  * El 70% de las toxinas que se desprenden de los tiraderos\nde basura provienen de los desechos electrónicos.
  * Son fuentes cancerigenas para el ser humano.
  * Muchos materiales utilizados para fabricar dispositivos\ninformáticos pueden ser recuperados en el reciclaje.
  * Se pueden reducir los costos de construcción de nuevos\nsistemas.
 * En 2016 se implementó una nueva ley que dice que cada país debe recoger\n45 toneladas de residuos electrónicos por cada 100 toneladas de\nproductos electrónicos puestos en venta.
@endmindmap
```
